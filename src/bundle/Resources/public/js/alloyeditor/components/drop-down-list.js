import React from 'react';
import DropDownListItem from './drop-down-list-item';

class DropDownList extends React.Component {

    /**
     * Lifecycle. Invoked once, both on the client and server, immediately before the initial rendering occurs.
     *
     * @method componentWillMount
     */
    componentWillMount() {
        this.callback = this.props.callback;
        this.items = (this.props.items && this.props.items.length) ? this.props.items : [];
    }

    /**
     * Lifecycle. Renders the UI of the button.
     *
     * @method render
     * @return {Object} The content which should be rendered.
     */
    render() {
        return (
            <div
                className="ae-dropdown ae-arrow-box ae-arrow-box-top-left"
                tabIndex="0"
            >
                <ul className="ae-listbox" role="listbox">
                    {this.renderItems()}
                </ul>
            </div>
        );
    }

    renderItems() {
        return this.items.map(item => {
            return (
                <li key={item.id} role="option">
                    <DropDownListItem
                        isActive={this.props.activeItem == item.id}
                        title={item.title}
                        callback={() => this.callback(item)}
                    />
                </li>
            );
        });
    }
}

export default DropDownList;