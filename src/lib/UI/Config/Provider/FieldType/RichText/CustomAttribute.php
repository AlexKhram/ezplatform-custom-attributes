<?php

namespace ContextualCode\EzPlatformCustomAttributes\UI\Config\Provider\FieldType\RichText;

use EzSystems\EzPlatformAdminUi\UI\Config\ProviderInterface;

/**
 * Provide information about RichText Custom Attributes.
 */
class CustomAttribute implements ProviderInterface
{
    /**
     * @var array
     */
    private $customAttributes;

    /**
     * @param array $customAttributes
     */
    public function __construct(array $customAttributes)
    {
        $this->customAttributes = $customAttributes;
    }

    /**
     * @return array RichText Custom Attributes config
     */
    public function getConfig(): array
    {
        return $this->customAttributes;
    }
}
