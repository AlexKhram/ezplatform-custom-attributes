<?php

namespace ContextualCode\EzPlatformCustomAttributes\UI\Config\Provider\FieldType\RichText;

use EzSystems\EzPlatformAdminUi\UI\Config\ProviderInterface;

/**
 * Provide information about RichText Embed Views.
 */
class EmbedViews implements ProviderInterface
{
    /**
     * @var array
     */
    private $embedViews;

    /**
     * @param array $embedViews
     */
    public function __construct(array $embedViews)
    {
        $this->embedViews = $embedViews;
    }

    /**
     * @return array RichText Embed Views config
     */
    public function getConfig(): array
    {
        return $this->embedViews;
    }
}
